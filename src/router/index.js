import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Customer from '@/components/Customer'
import Transaksi from '@/components/Transaksi'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    }, 
    {
        path: '/customer',
        name: 'Customer',
        component: Customer
    }, 
    {
        path: '/transaksi',
        name: 'Transaksi',
        component: Transaksi
    }, 
    { path: '*', component: HelloWorld }
  ]
})