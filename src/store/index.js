import Vue from 'vue'
import Vuex from 'vuex'

import VuexPersistence from 'vuex-persist'

const vuexLocal = new VuexPersistence({
  key: 'vuex',
  storage: window.localStorage
})

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
      accessToken: null,
      menuSamping: [
        { icon: 'home', text: 'Home' , path: '/', authenticated: false},
        { icon: 'contacts', text: 'Customer' , path: '/customer', authenticated: true},
        { icon: 'history', text: 'Transaksi' , path: '/transaksi', authenticated: true}
      ]
    }, 
    getters: {
      accessToken(state) {
          return state.accessToken
      }, 
      sudahLogin(state) {
          return state.accessToken != null 
              && state.accessToken != undefined
              && state.accessToken.length > 0
      }, 
      menuSamping: (state, getters) => {
        return state.menuSamping.filter( x => {
            return getters.sudahLogin ? x.authenticated : !x.authenticated
        })
      }
    }, 
    mutations: {
      accessToken(state, data){
          state.accessToken = data
      }
    }, 
    plugins: [vuexLocal.plugin]
  })